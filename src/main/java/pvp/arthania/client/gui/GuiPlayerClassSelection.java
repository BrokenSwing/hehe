package pvp.arthania.client.gui;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.util.ResourceLocation;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.network.ModNetwork;
import pvp.arthania.common.network.packets.CPacketChooseClassOrFaction;
import pvp.arthania.common.util.PlayerClass;

public class GuiPlayerClassSelection extends GuiScreen {

    public static final ResourceLocation BACKGROUND = new ResourceLocation(ArthaniaMod.MODID, "textures/gui/player_class_selection.png");

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        this.mc.getTextureManager().bindTexture(BACKGROUND);
        Gui.drawScaledCustomSizeModalRect((this.width - 256) / 2, (this.height - 192) / 2, 0, 0, 835, 579, 256, 192, 835, 579);
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    public void initGui() {
        int y = 0;
        for(PlayerClass c : PlayerClass.values()) {
            this.addButton(new GuiButton(c.ordinal(), 5, y, c.name()));
            y += 25;
        }
    }

    @Override
    public void updateScreen() {
        if(mc.player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null).getPlayerClass() != null) {
            this.mc.displayGuiScreen(null);
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        ModNetwork.NETWORK.sendToServer(new CPacketChooseClassOrFaction(0, button.id));
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (keyCode == Keyboard.KEY_ESCAPE) {
            if (this.mc.world != null) {
                this.mc.world.sendQuittingDisconnectingPacket();
            }

            this.mc.loadWorld((WorldClient) null);
            this.mc.displayGuiScreen(new GuiMainMenu());
        }
    }

}
