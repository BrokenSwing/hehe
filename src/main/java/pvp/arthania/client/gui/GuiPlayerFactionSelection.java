package pvp.arthania.client.gui;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.multiplayer.WorldClient;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.network.ModNetwork;
import pvp.arthania.common.network.packets.CPacketChooseClassOrFaction;
import pvp.arthania.common.util.PlayerFaction;

public class GuiPlayerFactionSelection extends GuiScreen {
    
    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    public void initGui() {
        int y = 0;
        for(PlayerFaction c : PlayerFaction.values()) {
            this.addButton(new GuiButton(c.ordinal(), 5, y, c.name()));
            y += 25;
        }
    }

    @Override
    public void updateScreen() {
        if(mc.player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null).getPlayerFaction() != null) {
            this.mc.displayGuiScreen(null);
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        ModNetwork.NETWORK.sendToServer(new CPacketChooseClassOrFaction(1, button.id));
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (keyCode == Keyboard.KEY_ESCAPE) {
            if (this.mc.world != null) {
                this.mc.world.sendQuittingDisconnectingPacket();
            }

            this.mc.loadWorld((WorldClient) null);
            this.mc.displayGuiScreen(new GuiMainMenu());
        }
    }

}
