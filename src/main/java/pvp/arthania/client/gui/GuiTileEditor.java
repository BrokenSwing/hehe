package pvp.arthania.client.gui;

import java.io.IOException;
import java.util.LinkedList;
import java.util.function.BiConsumer;
import java.util.function.Function;

import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import pvp.arthania.common.network.ModNetwork;
import pvp.arthania.common.network.packets.CPacketUpdateTile;

public abstract class GuiTileEditor<T extends TileEntity> extends GuiScreen {

    protected final T                      tile;
    protected final LinkedList<Prop<T, ?>> properties;
    protected int                          selectedProp;
    protected GuiTextField                 valueField;

    public GuiTileEditor(T tile) {
        this.tile = tile;
        this.properties = new LinkedList<>();
        
        this.selectedProp = 0;
    }

    @Override
    public void initGui() {
        this.addButton(new GuiButton(0, this.width - 200, 0, "Modify value"));
        valueField = new GuiTextField(1, this.fontRenderer, this.width - 301, 0, 100, 18);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();

        for (int i = 0; i < this.properties.size(); i++) {
            Prop<T, ?> p = properties.get(i);
            String text = p.getText() + " : " + p.get(this.tile);

            if (i == selectedProp) {
                Gui.drawRect(0, 20 * i + 5, this.fontRenderer.getStringWidth(text) + 10, 20 * i + 25, 0xFFFFFFFF);
                Gui.drawRect(1, 20 * i + 6, this.fontRenderer.getStringWidth(text) + 9, 20 * i + 24, 0xFF000000);
            }

            this.drawString(this.fontRenderer, text, 5, 10 + 20 * i, 0xFFFFFF);
        }

        this.valueField.drawTextBox();
        super.drawScreen(mouseX, mouseY, partialTicks);
    }
    

    @Override
    public void actionPerformed(GuiButton button) {
        if (button.id == 0) {
            if (this.valueField.getText().isEmpty())
                return;
            Prop<T, ?> prop = this.properties.get(this.selectedProp);
            try {
                prop.convertAndSet(this.tile, this.valueField.getText());
                ModNetwork.NETWORK.sendToServer(this.getUpdatePacket(this.tile));
                this.valueField.setTextColor(0xFF00);
            } catch (Exception e) {
                this.valueField.setTextColor(0xFF0000);
            }
        }
    }
    
    public IMessage getUpdatePacket(T tile) {
        return new CPacketUpdateTile(tile);
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);
        if (this.valueField.textboxKeyTyped(typedChar, keyCode)) {
            this.valueField.setTextColor(0xFFFFFF);
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.valueField.mouseClicked(mouseX, mouseY, mouseButton);
        for (int i = 0; i < this.properties.size(); i++) {
            Prop<T, ?> p = this.properties.get(i);
            int width = this.fontRenderer.getStringWidth(p.getText() + " : " + p.get(this.tile));
            if (mouseX >= 0 && mouseX <= width + 10 && mouseY >= i * 20 + 5 && mouseY < i * 20 + 24) {
                selectedProp = i;
                break;
            }
        }
    }

    protected static class Prop<T, A> {

        private final String              text;
        private final Function<T, A>      getter;
        private final BiConsumer<T, A>    setter;
        private final Function<String, A> converter;

        public Prop(String text, Function<T, A> getter, BiConsumer<T, A> setter, Function<String, A> converter) {
            this.text = text;
            this.getter = getter;
            this.setter = setter;
            this.converter = converter;
        }

        public void convertAndSet(T tile, String str) throws Exception {
            this.setter.accept(tile, this.converter.apply(str));
        }

        public String getText() {
            return text;
        }

        public A get(T tile) {
            return getter.apply(tile);
        }

    }

}
