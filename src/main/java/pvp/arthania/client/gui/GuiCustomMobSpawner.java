package pvp.arthania.client.gui;

import net.minecraft.util.ResourceLocation;
import pvp.arthania.common.tile.TileMobSpawner;

public class GuiCustomMobSpawner extends GuiTileEditor<TileMobSpawner> {

    public GuiCustomMobSpawner(TileMobSpawner tile) {
        super(tile);
        properties.add(new Prop<>("Activated", TileMobSpawner::isActive, TileMobSpawner::setActive, Boolean::valueOf));
        properties.add(new Prop<>("Dungeon mode", TileMobSpawner::isDungeonMode, TileMobSpawner::setDungeonMode, Boolean::valueOf));
        properties.add(new Prop<>("Is boss", TileMobSpawner::isBoss, TileMobSpawner::setBoss, Boolean::valueOf));
        properties.add(new Prop<>("Spawn radius", TileMobSpawner::getSpawnAreaRadius, TileMobSpawner::setSpawnAreaRadius, Integer::parseInt));
        properties.add(new Prop<>("Spawn intensity", TileMobSpawner::getSpawnIntensity, TileMobSpawner::setSpawnIntensity, Integer::parseInt));
        properties.add(new Prop<>("Time between two spawn (ticks)", TileMobSpawner::getTimeBetweenTwoSpawns, TileMobSpawner::setTimeBetweenTwoSpawns, Integer::parseInt));
        properties.add(new Prop<>("Entity health", TileMobSpawner::getEntityHealth, TileMobSpawner::setEntityHealth, Integer::parseInt));
        properties.add(new Prop<>("Entity strength", TileMobSpawner::getEntityAttackDamages, TileMobSpawner::setEntityAttackDamages, Integer::parseInt));
        properties.add(new Prop<>("Entity custom name", TileMobSpawner::getEntityCustomName, TileMobSpawner::setEntityCustomName, String::valueOf));
        properties.add(new Prop<>("Entity id", TileMobSpawner::getEntityId, TileMobSpawner::setEntityId, ResourceLocation::new));
    }

}
