package pvp.arthania.client.gui;

import java.util.HashSet;
import java.util.Set;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import pvp.arthania.common.crafting.JobCrafting;
import pvp.arthania.common.crafting.JobRecipe;
import pvp.arthania.common.util.PlayerJob;

public class GuiJobCrafting extends GuiScreen {

    protected final ResourceLocation background;
    protected final PlayerJob        job;
    protected Set<JobRecipe>         recipes;

    public GuiJobCrafting(PlayerJob job, ResourceLocation background) {
        this.background = background;
        this.job = job;
        this.recipes = JobCrafting.RECIPES.get(job);
        if (recipes == null) {
            recipes = new HashSet<>();
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        int i = 0;
        for (JobRecipe recipe : recipes) {
           RenderHelper.enableGUIStandardItemLighting();
           int x = 5;
           for(ItemStack ingredient : recipe.getIngredients()) {
               this.mc.getRenderItem().renderItemIntoGUI(ingredient, x, i * 30);
               x += 30;
           }
           
           this.drawString(this.fontRenderer, "->", x , i * 30 + 5, 0xFFFFFF);
           
           x += 20;
           
           for(ItemStack result : recipe.getResults()) {
               this.mc.getRenderItem().renderItemIntoGUI(result, x, i * 30);
               x += 30;
           }
        }
        if(recipes.isEmpty()) {
            this.drawString(this.fontRenderer, "No recipes", 5, 5, 0xFFFFFF);
        }
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

}
