package pvp.arthania.client.gui;

import java.io.IOException;

import org.lwjgl.opengl.GL11;

import factionmod.api.FactionModAPI.GuildApi;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.capability.IPlayerState;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.util.LevelCurve;

public class GuiPlayerProfile extends GuiScreen {

    public static final ResourceLocation BACKGROUND     = new ResourceLocation(ArthaniaMod.MODID + ":textures/gui/player_information.png");
    public static final ResourceLocation TEXTURE_XP_BAR = new ResourceLocation(ArthaniaMod.MODID + ":textures/gui/xp_bar.png");
    protected static final int           X_SIZE         = 256;
    protected static final int           Y_SIZE         = 192;

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        IPlayerState state = mc.player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null);

        int x = (this.width - X_SIZE) / 2;
        int y = (this.height - Y_SIZE) / 2;

        this.mc.getTextureManager().bindTexture(BACKGROUND);
        this.drawTexturedModalRect(x, y, 0, 0, X_SIZE, Y_SIZE);

        this.mc.getTextureManager().bindTexture(TEXTURE_XP_BAR);
        long xpNow = state.getXp();
        double xpToUp = LevelCurve.needExperienceToLevelUp(state.getLevel());
        Gui.drawScaledCustomSizeModalRect(x + 14, y + 165, 0, 0, ((int) ((xpNow / xpToUp) * 86)), 13, ((int) ((xpNow / xpToUp) * 86)), 13, 86, 13);

        GL11.glColor4d(1, 1, 1, 1);
        drawEntityOnScreen(x + 55, y + 130, 60, partialTicks, partialTicks, mc.player);
        this.drawCenteredString(this.fontRenderer, mc.player.getName(), this.width / 2 - 72, this.height / 2 + 45, 16777215);
        this.drawCenteredString(this.fontRenderer, I18n.format("gui.player.name"), this.width / 2 + 45, this.height / 2 - 85, 16777215);
        this.drawString(this.fontRenderer, I18n.format("gui.player.level", state.getLevel()), this.width / 2 - 15, this.height / 2 - 65, 16777215);
        this.drawString(this.fontRenderer, I18n.format("gui.player.class", state.getPlayerClass().name()), this.width / 2 - 15, this.height / 2 - 53, 16777215);
        this.drawString(this.fontRenderer, I18n.format("gui.player.faction", state.getPlayerFaction().name()), this.width / 2 - 15, this.height / 2 - 41, 16777215);
        if (GuildApi.getGuildOf(mc.player.getUniqueID()) == null) {
            this.drawString(this.fontRenderer, I18n.format("gui.player.no.guild"), this.width / 2 - 15, this.height / 2 - 29, 16777215);
        } else {
            this.drawString(this.fontRenderer, I18n.format("gui.player.guild", GuildApi.getGuildOf(mc.player.getUniqueID())), this.width / 2 - 15, this.height / 2 - 29, 16777215);
        }
        if (state.getMoney() <= 1) {
            this.drawString(this.fontRenderer, "Ata: " + state.getMoney(), this.width / 2 - 15, this.height / 2 - 17, 16777215);
        } else {
            this.drawString(this.fontRenderer, "Atas: " + state.getMoney(), this.width / 2 - 15, this.height / 2 - 17, 16777215);
        }
        GL11.glColor4d(1, 1, 1, 1);

        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    public void initGui() {
        this.buttonList.clear();
        this.buttonList.add(new GuiButton(0, this.width / 2 - 10, this.height / 2 + 15, 120, 20, I18n.format("gui.button.group")));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 10, this.height / 2 + 40, 120, 20, I18n.format("gui.button.jobs")));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 10, this.height / 2 + 65, 120, 20, I18n.format("gui.button.exit")));
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        if (button.id == 0) {
            // this.mc.displayGuiScreen(new GuiGroupInformation());
        }

        if (button.id == 1) {
            // this.mc.displayGuiScreen(new GuiJobsInformation());
        }

        if (button.id == 2) {
            this.mc.displayGuiScreen(null);

            if (this.mc.currentScreen == null) {
                this.mc.setIngameFocus();
            }
        }
    }

    public static void drawEntityOnScreen(int posX, int posY, int scale, float mouseX, float mouseY, EntityLivingBase ent) {
        GlStateManager.enableColorMaterial();
        GlStateManager.pushMatrix();
        GlStateManager.translate((float) posX, (float) posY, 50.0F);
        GlStateManager.scale((float) (-scale), (float) scale, (float) scale);
        GlStateManager.rotate(180.0F, 0.0F, 0.0F, 1.0F);
        float f = ent.renderYawOffset;
        float f1 = ent.rotationYaw;
        float f2 = ent.rotationPitch;
        float f3 = ent.prevRotationYawHead;
        float f4 = ent.rotationYawHead;
        GlStateManager.rotate(135.0F, 0.0F, 1.0F, 0.0F);
        RenderHelper.enableStandardItemLighting();
        GlStateManager.rotate(-135.0F, 0.0F, 1.0F, 0.0F);
        GlStateManager.rotate(-((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
        ent.renderYawOffset = (float) Math.atan((double) (mouseX / 40.0F)) * 20.0F;
        ent.rotationYaw = (float) Math.atan((double) (mouseX / 40.0F)) * 0.0F;
        ent.rotationPitch = -((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F;
        ent.rotationYawHead = ent.rotationYaw;
        ent.prevRotationYawHead = ent.rotationYaw;
        GlStateManager.translate(0.0F, 0.0F, 0.0F);
        RenderManager rendermanager = Minecraft.getMinecraft().getRenderManager();
        rendermanager.renderEntity(ent, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F, false);
        ent.renderYawOffset = f;
        ent.rotationYaw = f1;
        ent.rotationPitch = f2;
        ent.prevRotationYawHead = f3;
        ent.rotationYawHead = f4;
        GlStateManager.popMatrix();
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableRescaleNormal();
        GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GlStateManager.disableTexture2D();
        GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
    }

}
