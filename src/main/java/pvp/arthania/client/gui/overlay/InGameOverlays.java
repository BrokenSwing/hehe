package pvp.arthania.client.gui.overlay;

import org.lwjgl.opengl.GL11;

import com.mojang.util.UUIDTypeAdapter;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.capability.IPlayerState;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.util.LevelCurve;

@EventBusSubscriber(modid = ArthaniaMod.MODID, value = Side.CLIENT)
public class InGameOverlays {

    private static final ResourceLocation TEXTURE     = new ResourceLocation(ArthaniaMod.MODID + ":textures/gui/info_player_empty.png");
    private static final ResourceLocation TEXTURELIFE = new ResourceLocation(ArthaniaMod.MODID + ":textures/gui/life_bar.png");
    private static final ResourceLocation TEXTUREMANA = new ResourceLocation(ArthaniaMod.MODID + ":textures/gui/mana_bar.png");
    private static final ResourceLocation TEXTUREXP   = new ResourceLocation(ArthaniaMod.MODID + ":textures/gui/xp_bar_ingame.png");

    /*
     * Change render ingame
     */

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public static void renderInGameMenu(RenderGameOverlayEvent event) {

        switch (event.getType()) {
            case ARMOR:
            case EXPERIENCE:
            case FOOD:
            case HEALTHMOUNT:
            case AIR:
            case HELMET:
            case JUMPBAR:
            case POTION_ICONS:
                event.setCanceled(true);
                break;
            case HEALTH:
                event.setCanceled(true);
                IPlayerState state = Minecraft.getMinecraft().player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null);
                if (Minecraft.getMinecraft().gameSettings.showDebugInfo)
                    return;

                // INFO PLAYER EMPTY //
                Minecraft.getMinecraft().getTextureManager().bindTexture(TEXTURE);
                GL11.glPushMatrix();
                GL11.glScalef(0.27F, 0.27F, 0.27F);
                Gui.drawScaledCustomSizeModalRect(-3, -9, 0, 0, 1020, 270, 1020, 270, 1020, 270);
                GL11.glPopMatrix();

                // HEALTH //
                Minecraft.getMinecraft().getTextureManager().bindTexture(TEXTURELIFE);
                GL11.glPushMatrix();
                GL11.glScalef(0.27F, 0.27F, 0.27F);
                Gui.drawScaledCustomSizeModalRect(220, 43, 0, 0, ((int) ((Minecraft.getMinecraft().player.getHealth() / Minecraft.getMinecraft().player.getMaxHealth()) * 757)), 30,
                        ((int) ((Minecraft.getMinecraft().player.getHealth() / Minecraft.getMinecraft().player.getMaxHealth()) * 757)), 30, 757, 30);
                GL11.glPopMatrix();

                // MANA //
                Minecraft.getMinecraft().getTextureManager().bindTexture(TEXTUREMANA);
                GL11.glPushMatrix();
                GL11.glScalef(0.27F, 0.27F, 0.27F);
                Gui.drawScaledCustomSizeModalRect(244, 85, 0, 0, 628, 30, 628, 30, 628, 30);
                GL11.glPopMatrix();

                // EXPERIENCE //
                Minecraft.getMinecraft().getTextureManager().bindTexture(TEXTUREXP);
                GL11.glPushMatrix();
                GL11.glScalef(0.27F, 0.27F, 0.27F);
                Gui.drawScaledCustomSizeModalRect(251, 122, 0, 0, ((int) ((state.getXp() / LevelCurve.needExperienceToLevelUp(state.getLevel())) * 532)), 14,
                        ((int) ((state.getXp() / LevelCurve.needExperienceToLevelUp(state.getLevel())) * 532)), 14, 757, 14);
                GL11.glPopMatrix();

                // ARMOR //
                Minecraft.getMinecraft().getRenderItem().renderItemAndEffectIntoGUI(Minecraft.getMinecraft().player.inventory.armorInventory.get(3), 72, 41);
                Minecraft.getMinecraft().getRenderItem().renderItemAndEffectIntoGUI(Minecraft.getMinecraft().player.inventory.armorInventory.get(2), 94, 41);
                Minecraft.getMinecraft().getRenderItem().renderItemAndEffectIntoGUI(Minecraft.getMinecraft().player.inventory.armorInventory.get(1), 117, 41);
                Minecraft.getMinecraft().getRenderItem().renderItemAndEffectIntoGUI(Minecraft.getMinecraft().player.inventory.armorInventory.get(0), 139, 41);

                // PLAYER HEAD //
                GL11.glPushMatrix();
                GL11.glScalef(7F, 7F, 7F);
                bindFace(Minecraft.getMinecraft().player.getName(), Minecraft.getMinecraft().player.getUniqueID().toString());
                Gui.drawScaledCustomSizeModalRect(3, 2, 4, 4, 4, 4, 4, 4, 32, 32);
                GL11.glPopMatrix();
                GL11.glColor4d(1, 1, 1, 1);
            default:
        }
    }

    /*
     * Get face for player
     */

    public static void bindFace(String name, String uuid) {
        ResourceLocation resourcelocation = AbstractClientPlayer.getLocationSkin(name);

        if (resourcelocation == null) {
            resourcelocation = DefaultPlayerSkin.getDefaultSkin(UUIDTypeAdapter.fromString(uuid));
        }

        AbstractClientPlayer.getDownloadImageSkin(resourcelocation, name);
        Minecraft.getMinecraft().getTextureManager().bindTexture(resourcelocation);
    }

}
