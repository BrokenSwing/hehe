package pvp.arthania.client.render.tesr;

import com.google.common.base.Function;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.model.animation.FastTESR;

public class ConditionedTESR extends FastTESR<TileEntity> {

    protected static BlockRendererDispatcher blockRenderer = null;

    protected final Function<Minecraft, Boolean> condition;

    public ConditionedTESR(Function<Minecraft, Boolean> condition) {
        this.condition = condition;
    }

    @Override
    public void renderTileEntityFast(TileEntity te, double x, double y, double z, float partialTicks, int destroyStage, float partial, BufferBuilder buffer) {
        GlStateManager.pushMatrix();

        if (condition.apply(Minecraft.getMinecraft())) {
            if (blockRenderer == null)
                blockRenderer = Minecraft.getMinecraft().getBlockRendererDispatcher();

            final BlockPos pos = te.getPos();
            final IBlockAccess world = MinecraftForgeClient.getRegionRenderCache(te.getWorld(), pos);
            final IBlockState state = world.getBlockState(pos);

            buffer.setTranslation(x - pos.getX(), y - pos.getY(), z - pos.getZ());

            final IBakedModel model = blockRenderer.getBlockModelShapes().getModelForState(state);

            blockRenderer.getBlockModelRenderer().renderModel(world, model,
                    state.getBlock().getExtendedState(state, world, pos), pos, buffer, true);
        }

        GlStateManager.popMatrix();
    }

}
