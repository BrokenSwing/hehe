package pvp.arthania.client.handler;

import java.util.concurrent.atomic.AtomicInteger;

import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraftforge.fml.relauncher.Side;
import pvp.arthania.client.gui.GuiPlayerClassSelection;
import pvp.arthania.client.gui.GuiPlayerFactionSelection;
import pvp.arthania.client.gui.GuiPlayerProfile;
import pvp.arthania.client.proxy.ClientProxy;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.capability.IPlayerState;
import pvp.arthania.common.capability.PlayerStateProvider;

@EventBusSubscriber(modid = ArthaniaMod.MODID, value = Side.CLIENT)
public class GuiOpeningHandler {

    // Used to avoid blinking gui on respawn
    private static AtomicInteger tickBuffer = new AtomicInteger(0);

    @SubscribeEvent
    public static void openClassAndFactionGuis(PlayerTickEvent event) {
        if (Minecraft.getMinecraft().currentScreen == null) {
            if (tickBuffer.get() > 10) {
                tickBuffer.set(0);
                IPlayerState state = event.player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null);
                if (state.getPlayerClass() == null)
                    Minecraft.getMinecraft().addScheduledTask(() -> Minecraft.getMinecraft().displayGuiScreen(new GuiPlayerClassSelection()));
                else if (state.getPlayerFaction() == null)
                    Minecraft.getMinecraft().addScheduledTask(() -> Minecraft.getMinecraft().displayGuiScreen(new GuiPlayerFactionSelection()));
            } else {
                tickBuffer.incrementAndGet();
            }
        }
    }
    
    @SubscribeEvent
    public static void keyInput(KeyInputEvent event) {
        if(ClientProxy.PLAYER_PROFILE_KEY.isPressed()) {
            Minecraft.getMinecraft().displayGuiScreen(new GuiPlayerProfile());
        }
    }

}
