package pvp.arthania.client.proxy;

import java.io.File;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import pvp.arthania.client.config.ClientConfig;
import pvp.arthania.client.render.tesr.ConditionedTESR;
import pvp.arthania.common.config.CommonConfig;
import pvp.arthania.common.item.ModItems;
import pvp.arthania.common.proxy.CommonProxy;
import pvp.arthania.common.tile.TileMobSpawner;

public class ClientProxy extends CommonProxy {
    
    public static final KeyBinding PLAYER_PROFILE_KEY = new KeyBinding("key.player_profile.name", KeyConflictContext.IN_GAME, Keyboard.KEY_P, "Arthania");

    @Override
    protected CommonConfig createConfig(File configFile, String version) {
        return new ClientConfig(configFile, version);
    }

    @Override
    public void init(FMLInitializationEvent event) {
        super.init(event);
        ClientRegistry.registerKeyBinding(PLAYER_PROFILE_KEY);
    }

    @Override
    public void postInit(FMLPostInitializationEvent event) {
        super.postInit(event);
        registerTESR();
    }
    
    protected void registerTESR() {
        ClientRegistry.bindTileEntitySpecialRenderer(TileMobSpawner.class, new ConditionedTESR(mc -> mc.player.getHeldItemMainhand().getItem() == ModItems.ADMIN_WAND));
    }

}
