package pvp.arthania.client.config;

import java.io.File;

import pvp.arthania.common.config.CommonConfig;

public class ClientConfig extends CommonConfig {

    public ClientConfig(File file, String version) {
        super(file, version);
    }

}
