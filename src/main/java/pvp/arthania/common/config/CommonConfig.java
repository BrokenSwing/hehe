package pvp.arthania.common.config;

import java.io.File;

import net.minecraftforge.common.config.Configuration;

public abstract class CommonConfig {
    
    protected final Configuration cfg;
    
    public CommonConfig(File file, String version) {
        cfg = new Configuration(file, version);
    }
    
    public void load() {
        
    }
    
    public void reload() {
        this.load();
    }

}
