package pvp.arthania.common.network.util;

import javax.annotation.Nullable;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface IGuiProvider {
    
    @SideOnly(Side.CLIENT)
    public GuiScreen getGui(World world, BlockPos pos, EntityPlayer player, @Nullable TileEntity tile);
    
    default public Container getContainer(World world, BlockPos pos, EntityPlayer player, @Nullable TileEntity tile) {
        return new EmptyContainer(pos, 5d);
    }

}
