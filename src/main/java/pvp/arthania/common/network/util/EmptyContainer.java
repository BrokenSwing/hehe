package pvp.arthania.common.network.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.math.BlockPos;

public class EmptyContainer extends Container {

    private final BlockPos pos;
    private final double   useableRange;

    public EmptyContainer(BlockPos pos, double useableRange) {
        this.pos = pos;
        this.useableRange = useableRange;
    }

    @Override
    public boolean canInteractWith(EntityPlayer player) {
        return player.getPosition().distanceSq(this.pos) < useableRange * useableRange;
    }

}
