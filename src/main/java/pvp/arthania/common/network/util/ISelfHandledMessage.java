package pvp.arthania.common.network.util;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.function.BiConsumer;

import com.google.common.base.Function;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import pvp.arthania.common.network.ModNetwork;

public interface ISelfHandledMessage<A extends ISelfHandledMessage<A>> extends IMessage, IMessageHandler<A, IMessage> {

    public IMessage handle(MessageContext context);

    public EnumSet<Side> getTargetedSides();

    @Override
    default public IMessage onMessage(A message, MessageContext ctx) {
        return message.handle(ctx);
    }

    ///////////////////////
    // Quick Use Methods //
    ///////////////////////

    default public void writeString(ByteBuf to, String text) {
        ByteBufUtils.writeUTF8String(to, text);
    }

    default public String readString(ByteBuf from) {
        return ByteBufUtils.readUTF8String(from);
    }

    default public void writeTag(ByteBuf to, NBTTagCompound nbt) {
        ByteBufUtils.writeTag(to, nbt);
    }

    default public NBTTagCompound readTag(ByteBuf from) {
        return ByteBufUtils.readTag(from);
    }

    default public void writeItemStack(ByteBuf to, ItemStack stack) {
        ByteBufUtils.writeItemStack(to, stack);
    }

    default public ItemStack readItemStack(ByteBuf from) {
        return ByteBufUtils.readItemStack(from);
    }

    default public <TYPE> void writeCollection(ByteBuf to, Collection<TYPE> collection, BiConsumer<ByteBuf, TYPE> processor) {
        to.writeInt(collection.size());
        collection.forEach(e -> processor.accept(to, e));
    }

    default public <TYPE> Collection<TYPE> readCollection(ByteBuf from, Function<ByteBuf, TYPE> processor) {
        LinkedList<TYPE> list = new LinkedList<>();
        int count = from.readInt();
        for (int i = 0; i < count; i++) {
            list.add(processor.apply(from));
        }
        return list;
    }

    default public <TYPE> void writeArray(ByteBuf to, TYPE[] elements, BiConsumer<ByteBuf, TYPE> processor) {
        LinkedList<TYPE> list = new LinkedList<>();
        Collections.addAll(list, elements);
        writeCollection(to, list, processor);
    }

    @SuppressWarnings("unchecked")
    default public <TYPE> TYPE[] readArray(ByteBuf from, Function<ByteBuf, TYPE> processor) {
        Collection<TYPE> collection = readCollection(from, processor);
        return (TYPE[]) collection.toArray();
    }

    ////////////////
    // SEND STUFF //
    ////////////////

    default public void sendToServer() {
        ModNetwork.NETWORK.sendToServer(this);
    }

    default public void sendToPlayer(EntityPlayerMP player) {
        ModNetwork.NETWORK.sendTo(this, player);
    }

    default public void sendToAll() {
        ModNetwork.NETWORK.sendToAll(this);
    }

}
