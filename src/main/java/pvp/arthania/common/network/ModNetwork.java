package pvp.arthania.common.network;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.network.packets.CPacketChooseClassOrFaction;
import pvp.arthania.common.network.packets.CPacketUpdateTile;
import pvp.arthania.common.network.packets.SPacketToastGui;
import pvp.arthania.common.network.packets.SPacketUpdateCapability;
import pvp.arthania.common.network.util.IGuiProvider;
import pvp.arthania.common.network.util.ISelfHandledMessage;

public class ModNetwork implements IGuiHandler {

    public static final SimpleNetworkWrapper NETWORK = new SimpleNetworkWrapper(ArthaniaMod.MODID);

    public static void registerPackets() {
        register(CPacketUpdateTile.class);
        register(SPacketUpdateCapability.class);
        register(SPacketToastGui.class);
        register(CPacketChooseClassOrFaction.class);
    }

    protected static int packetsCount = 0;

    protected static <A extends ISelfHandledMessage<A>> void register(Class<A> packet) {
        A a;
        try {
            a = packet.newInstance();
            for (Side side : a.getTargetedSides())
                NETWORK.registerMessage(packet, packet, packetsCount++, side);
        } catch (Exception e) {
            ArthaniaMod.logger.error("Error while registering message {}. It must have a public args-less constructor.", packet.getSimpleName());
        }
    }

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        BlockPos pos = new BlockPos(x, y, z);
        if (ID == 0 && world.getBlockState(pos).getBlock() instanceof IGuiProvider) {
            return ((IGuiProvider) world.getBlockState(pos).getBlock()).getContainer(world, pos, player, world.getTileEntity(pos));
        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        BlockPos pos = new BlockPos(x, y, z);
        if (ID == 0 && world.getBlockState(pos).getBlock() instanceof IGuiProvider) {
            return ((IGuiProvider) world.getBlockState(pos).getBlock()).getGui(world, pos, player, world.getTileEntity(pos));
        }
        return null;
    }

}
