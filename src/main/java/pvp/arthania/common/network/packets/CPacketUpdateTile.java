package pvp.arthania.common.network.packets;

import java.util.EnumSet;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import pvp.arthania.common.item.ModItems;
import pvp.arthania.common.network.util.ISelfHandledMessage;

public class CPacketUpdateTile implements ISelfHandledMessage<CPacketUpdateTile> {

    protected NBTTagCompound nbt;

    public CPacketUpdateTile() {}

    public CPacketUpdateTile(TileEntity tile) {
        this.nbt = tile.writeToNBT(new NBTTagCompound());
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.nbt = this.readTag(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        this.writeTag(buf, this.nbt);
    }

    @Override
    public IMessage handle(MessageContext context) {
        EntityPlayerMP player = context.getServerHandler().player;
        if (player.getHeldItemMainhand().getItem() == ModItems.ADMIN_WAND) {
            TileEntity tile = player.getEntityWorld().getTileEntity(new BlockPos(nbt.getInteger("x"), nbt.getInteger("y"), nbt.getInteger("z")));
            if (tile != null) {
                tile.readFromNBT(this.nbt);
                tile.markDirty();
            }
        }
        return null;
    }

    @Override
    public EnumSet<Side> getTargetedSides() {
        return EnumSet.of(Side.SERVER);
    }

}
