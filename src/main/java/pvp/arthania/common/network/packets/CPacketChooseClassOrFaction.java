package pvp.arthania.common.network.packets;

import java.util.EnumSet;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import pvp.arthania.common.capability.IPlayerState;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.network.util.ISelfHandledMessage;
import pvp.arthania.common.util.PlayerClass;
import pvp.arthania.common.util.PlayerFaction;

public class CPacketChooseClassOrFaction implements ISelfHandledMessage<CPacketChooseClassOrFaction> {

    protected int type;
    protected int choosen;

    public CPacketChooseClassOrFaction() {}

    /**
     * @param type}
     *            0 for {@link PlayerClass}, 1 for {@link PlayerFaction
     * @param choosen
     *            {@link Enum#ordinal()}
     */
    public CPacketChooseClassOrFaction(int type, int choosen) {
        this.type = type;
        this.choosen = choosen;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        byte sum = buf.readByte();
        this.type = sum & 1;
        this.choosen = sum >> 1;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeByte((choosen << 1) | type);
    }

    @Override
    public IMessage handle(MessageContext context) {
        EntityPlayerMP player = context.getServerHandler().player;
        IPlayerState state = player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null);
        if (type == 0) {
            if (state.getPlayerClass() == null && this.choosen >= 0 && this.choosen < PlayerClass.values().length) {
                state.setPlayerClass(PlayerClass.values()[this.choosen]);
            }
        } else if (type == 1) {
            if (state.getPlayerFaction() == null && this.choosen >= 0 && this.choosen < PlayerFaction.values().length) {
                state.setPlayerFaction(PlayerFaction.values()[this.choosen]);
            }
        }
        return null;
    }

    @Override
    public EnumSet<Side> getTargetedSides() {
        return EnumSet.of(Side.SERVER);
    }

}
