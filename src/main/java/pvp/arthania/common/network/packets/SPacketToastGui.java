package pvp.arthania.common.network.packets;

import java.util.EnumSet;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.toasts.SystemToast;
import net.minecraft.client.gui.toasts.SystemToast.Type;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import pvp.arthania.common.network.util.ISelfHandledMessage;

public class SPacketToastGui implements ISelfHandledMessage<SPacketToastGui> {
    protected ITextComponent title;
    protected ITextComponent subtitle;

    public SPacketToastGui() {}

    public SPacketToastGui(ITextComponent title, ITextComponent subtitle) {
        this.title = title;
        this.subtitle = subtitle;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.title = ITextComponent.Serializer.jsonToComponent(this.readString(buf));
        this.subtitle = ITextComponent.Serializer.jsonToComponent(this.readString(buf));
    }

    @Override
    public void toBytes(ByteBuf buf) {
        this.writeString(buf, ITextComponent.Serializer.componentToJson(this.title));
        this.writeString(buf, ITextComponent.Serializer.componentToJson(this.subtitle));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IMessage handle(MessageContext context) {
        Minecraft.getMinecraft().getToastGui().add(new SystemToast(Type.TUTORIAL_HINT, this.title, this.subtitle));
        return null;
    }

    @Override
    public EnumSet<Side> getTargetedSides() {
        return EnumSet.of(Side.CLIENT);
    }

}
