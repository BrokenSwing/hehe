package pvp.arthania.common.network.packets;

import java.util.EnumSet;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import pvp.arthania.common.capability.IPlayerState;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.network.util.ISelfHandledMessage;

public class SPacketUpdateCapability implements ISelfHandledMessage<SPacketUpdateCapability> {

    private NBTTagCompound nbt;

    public SPacketUpdateCapability() {}

    public SPacketUpdateCapability(IPlayerState state) {
        this.nbt = (NBTTagCompound) PlayerStateProvider.PLAYER_STATE_CAPA.writeNBT(state, null);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.nbt = readTag(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        writeTag(buf, this.nbt);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IMessage handle(MessageContext context) {
        Minecraft.getMinecraft().addScheduledTask(() -> {
            PlayerStateProvider.PLAYER_STATE_CAPA
                    .readNBT(
                            Minecraft.getMinecraft().player
                                    .getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null),
                            null, this.nbt);

        });
        return null;
    }

    @Override
    public EnumSet<Side> getTargetedSides() {
        return EnumSet.of(Side.CLIENT);
    }

}
