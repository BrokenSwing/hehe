package pvp.arthania.common.tile;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants.NBT;

public class TileMobSpawner extends TileEntity implements ITickable {

    private boolean          isActive;
    private boolean          dungeonMode;
    private boolean          isBoss;
    private int              spawnAreaRadius;
    private int              timeBetweenTwoSpawns;
    private int              spawnIntensity;
    private int              entityHealth;
    private int              entityAttackDamages;
    private int              bossId;
    private ResourceLocation entityId;
    private String           entityCustomName;

    public TileMobSpawner() {
        super();
        isActive = false;
        dungeonMode = false;
        isBoss = false;
        spawnAreaRadius = 10;
        timeBetweenTwoSpawns = 5 * 20;
        ticksBeforeSpawn = timeBetweenTwoSpawns;
        spawnIntensity = 1;
        entityHealth = 20;
        entityAttackDamages = 2;
        bossId = -1;
        entityId = null;
        entityCustomName = "";
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        this.isActive = compound.getBoolean("isActive");
        this.dungeonMode = compound.getBoolean("dungeonMode");
        this.isBoss = compound.getBoolean("isBoss");
        this.spawnAreaRadius = compound.getInteger("areaOfEffect");
        this.timeBetweenTwoSpawns = compound.getInteger("timeBetweenTwoSpawn");
        this.spawnIntensity = compound.getInteger("numberSpawn");
        this.entityHealth = compound.getInteger("entityLife");
        this.entityAttackDamages = compound.getInteger("entityDamage");
        this.bossId = compound.getInteger("bossId");
        if (compound.getString("entityId").equals(""))
            this.entityId = null;
        else
            this.entityId = new ResourceLocation(compound.getString("entityId").split(":")[0], compound.getString("entityId").split(":")[1]);
        this.entityCustomName = compound.getString("entityName");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setBoolean("isActive", this.isActive);
        compound.setBoolean("dungeonMode", this.dungeonMode);
        compound.setBoolean("isBoss", this.isBoss);
        compound.setInteger("areaOfEffect", this.spawnAreaRadius);
        compound.setInteger("timeBetweenTwoSpawn", this.timeBetweenTwoSpawns);
        compound.setInteger("numberSpawn", this.spawnIntensity);
        compound.setInteger("entityLife", this.entityHealth);
        compound.setInteger("entityDamage", this.entityAttackDamages);
        compound.setInteger("bossId", this.bossId);
        if (this.entityId != null)
            compound.setString("entityId", this.entityId.toString());
        compound.setString("entityName", this.entityCustomName);
        return compound;
    }

    private boolean spawnedOnce = false;
    private int     ticksBeforeSpawn;

    @Override
    public void update() {
        if (this.world.isRemote || !this.isActive)
            return;

        if (this.dungeonMode && !spawnedOnce) {
            if (this.isBoss) {
                EntityLiving entity = spawnCreature(this.getWorld(), this.entityId, (double) this.getPos().getX() + 0.5D, (double) this.getPos().getY() + 0.5D, (double) this.getPos().getZ() + 0.5D);
                if (entity != null) {
                    entity.setCustomNameTag(this.entityCustomName);
                    entity.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.entityHealth);
                    if (entity.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE) != null)
                        entity.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(this.entityAttackDamages);
                    spawnedOnce = true;
                }
            } else {
                for (int i = 0; i < this.spawnIntensity; i++) {
                    EntityLiving entity = spawnCreature(this.getWorld(), this.entityId, (double) this.getPos().getX() + 0.5D, (double) this.getPos().getY() + 0.5D,
                            (double) this.getPos().getZ() + 0.5D);
                    if (entity != null) {
                        entity.setCustomNameTag(this.entityCustomName);
                        entity.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.entityHealth);
                        if (entity.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE) != null)
                            entity.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(this.entityAttackDamages);
                        spawnedOnce = true;
                    }
                }
            }
        } else {
            if (ticksBeforeSpawn-- < 0) {
                final BlockPos radius = new BlockPos(spawnAreaRadius, spawnAreaRadius, spawnAreaRadius);
                List<Entity> entities = this.world.getEntitiesWithinAABBExcludingEntity(null, new AxisAlignedBB(this.pos.subtract(radius), this.pos.add(radius)));
                if (entities.stream()
                        .filter(entity -> entity.getEntityData().hasKey("spawnerPos", NBT.TAG_COMPOUND) && NBTUtil.getPosFromTag(entity.getEntityData().getCompoundTag("spawnerPos")).equals(this.pos))
                        .collect(Collectors.toList())
                        .size() < this.spawnIntensity) {
                    EntityLiving entity = spawnCreature(this.getWorld(), this.entityId, (double) this.getPos().getX() + 0.5D, (double) this.getPos().getY() + 0.5D,
                            (double) this.getPos().getZ() + 0.5D);
                    if (entity != null) {
                        entity.getEntityData().setTag("spawnerPos", NBTUtil.createPosTag(this.pos));
                        entity.setCustomNameTag(this.entityCustomName);
                        entity.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.entityHealth);
                        if (entity.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE) != null)
                            entity.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(this.entityAttackDamages);
                        ticksBeforeSpawn = timeBetweenTwoSpawns;
                    } else {
                        ticksBeforeSpawn = -1;
                    }
                } else {
                    ticksBeforeSpawn = -1;
                }
            }
        }
    }

    @Nullable
    private EntityLiving spawnCreature(World worldIn, @Nullable ResourceLocation entityID, double x, double y, double z) {
        if (entityID != null) {

            Entity entity = EntityList.createEntityByIDFromName(entityID, worldIn);

            if (entity instanceof EntityLiving) {
                EntityLiving entityliving = (EntityLiving) entity;
                entity.setLocationAndAngles(x, y, z, MathHelper.wrapDegrees(worldIn.rand.nextFloat() * 360.0F), 0.0F);
                entityliving.rotationYawHead = entityliving.rotationYaw;
                entityliving.renderYawOffset = entityliving.rotationYaw;
                entityliving.onInitialSpawn(worldIn.getDifficultyForLocation(new BlockPos(entityliving)), (IEntityLivingData) null);
                worldIn.spawnEntity(entity);
                entityliving.playLivingSound();
                return entityliving;
            }

        }
        return null;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        return this.writeToNBT(new NBTTagCompound());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.handleUpdateTag(pkt.getNbtCompound());
    }

    public void setEntityCustomName(String name) {
        this.entityCustomName = name;
        this.markDirty();
    }

    public String getEntityCustomName() {
        return this.entityCustomName;
    }

    @Override
    public void markDirty() {
        super.markDirty();
        this.world.markAndNotifyBlock(this.pos, this.world.getChunkFromBlockCoords(this.pos), this.world.getBlockState(this.pos), this.world.getBlockState(this.pos), 3);
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
        this.markDirty();
    }

    public boolean isDungeonMode() {
        return dungeonMode;
    }

    public void setDungeonMode(boolean dungeonMode) {
        this.dungeonMode = dungeonMode;
        this.markDirty();
    }

    public boolean isBoss() {
        return isBoss;
    }

    public void setBoss(boolean isBoss) {
        this.isBoss = isBoss;
        this.markDirty();
    }

    public int getSpawnAreaRadius() {
        return spawnAreaRadius;
    }

    public void setSpawnAreaRadius(int spawnAreaRadius) {
        this.spawnAreaRadius = spawnAreaRadius;
        this.markDirty();
    }

    public int getTimeBetweenTwoSpawns() {
        return timeBetweenTwoSpawns;
    }

    public void setTimeBetweenTwoSpawns(int timeBetweenTwoSpawns) {
        this.timeBetweenTwoSpawns = timeBetweenTwoSpawns;
        this.markDirty();
    }

    public int getSpawnIntensity() {
        return spawnIntensity;
    }

    public void setSpawnIntensity(int spawnIntensity) {
        this.spawnIntensity = spawnIntensity;
        this.markDirty();
    }

    public int getEntityHealth() {
        return entityHealth;
    }

    public void setEntityHealth(int entityHealth) {
        this.entityHealth = entityHealth;
        this.markDirty();
    }

    public int getEntityAttackDamages() {
        return entityAttackDamages;
    }

    public void setEntityAttackDamages(int entityAttackDamages) {
        this.entityAttackDamages = entityAttackDamages;
        this.markDirty();
    }

    public int getBossId() {
        return bossId;
    }

    public void setBossId(int bossId) {
        this.bossId = bossId;
        this.markDirty();
    }

    public ResourceLocation getEntityId() {
        return entityId;
    }

    public void setEntityId(ResourceLocation entityId) {
        this.entityId = entityId;
        this.markDirty();
    }

}
