package pvp.arthania.common.block;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import pvp.arthania.client.gui.GuiJobCrafting;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.network.util.IGuiProvider;
import pvp.arthania.common.util.PlayerJob;

public class BlockJobCrafting extends BlockBase implements IGuiProvider {

    protected PlayerJob        job;
    protected ResourceLocation guiBackground;

    public BlockJobCrafting(String name, PlayerJob job) {
        super(Material.ROCK, name);
        this.job = job;
        this.guiBackground = new ResourceLocation(ArthaniaMod.MODID, "textures/gui/crafting/" + job.name().toLowerCase() + ".jpng");
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if(player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null).getJobs().contains(job)) {
            if(!world.isRemote) {
                player.openGui(ArthaniaMod.instance, 0, world, pos.getX(), pos.getY(), pos.getZ());
            }
            return true;
        }
        return false;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public GuiScreen getGui(World world, BlockPos pos, EntityPlayer player, TileEntity tile) {
        return new GuiJobCrafting(job, guiBackground);
    }

}
