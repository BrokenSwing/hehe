package pvp.arthania.common.block;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import pvp.arthania.common.capability.IPlayerState;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.util.LevelCurve;
import pvp.arthania.common.util.PlayerJob;

public class BlockJobResource extends BlockBase {

    private PlayerJob job;
    private int       requiredLevel;
    private int       experienceReward;
    private ItemStack drop;

    public BlockJobResource(String name, PlayerJob job, int requiredLevel, int expReward, ItemStack drop) {
        super(Material.ICE, name);
        this.job = job;
        this.requiredLevel = requiredLevel;
        this.experienceReward = expReward;
        this.drop = drop;
        this.setHardness(7F);
        this.setResistance(-1F);
    }

    @Override
    public boolean canHarvestBlock(IBlockAccess world, BlockPos pos, EntityPlayer player) {
        IPlayerState state = player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null);
        return super.canHarvestBlock(world, pos, player) && LevelCurve.getJobLevelFromExperience(state.getExperienceOf(job)) >= this.requiredLevel && state.getJobs().contains(this.job);
    }

    @Override
    public void harvestBlock(World worldIn, EntityPlayer player, BlockPos pos, IBlockState state, TileEntity te, ItemStack stack) {
        super.harvestBlock(worldIn, player, pos, state, te, stack);
        IPlayerState playerState = player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null);
        playerState.addExperienceTo(this.job, this.experienceReward);
    }

    @Override
    public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        drops.add(drop.copy());
    }

    public PlayerJob getJob() {
        return job;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public int getExperienceReward() {
        return experienceReward;
    }

}
