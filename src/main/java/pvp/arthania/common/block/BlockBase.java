package pvp.arthania.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import pvp.arthania.common.ArthaniaMod;

public class BlockBase extends Block {

    public BlockBase(Material materialIn, String name) {
        super(materialIn);
        this.setCreativeTab(ArthaniaMod.MOD_TAB);
        this.setRegistryName(ArthaniaMod.MODID, name);
        this.setUnlocalizedName(name);
    }

}
