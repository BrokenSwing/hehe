package pvp.arthania.common.block;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedHashMap;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import com.google.common.base.Function;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.JsonUtils;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.block.spawner.BlockMobSpawner;
import pvp.arthania.common.crafting.JobCrafting;
import pvp.arthania.common.item.ItemBase;
import pvp.arthania.common.item.ModItems;
import pvp.arthania.common.tile.TileMobSpawner;
import pvp.arthania.common.util.PlayerJob;

@EventBusSubscriber(modid = ArthaniaMod.MODID)
public class ModBlocks {

    public static final LinkedHashMap<Block, Item> BLOCKS = new LinkedHashMap<>();

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        register(new BlockMobSpawner("mob_spawner"));
        registerTile(TileMobSpawner.class, "mob_spawner");

        for (PlayerJob job : PlayerJob.values()) {
            register(new BlockJobCrafting("crafting_" + job.name().toLowerCase(), job));

            CraftingHelper.findFiles(ArthaniaMod.container, "assets/" + ArthaniaMod.MODID + "/resources/" + job.name().toLowerCase(), null, (root, file) -> {
                if (!"json".equals(FilenameUtils.getExtension(file.toString()))) {
                    return true;
                }

                BufferedReader reader = null;
                try {
                    reader = Files.newBufferedReader(file);
                    JsonObject json = JsonUtils.fromJson(JobCrafting.GSON, reader, JsonObject.class);
                    int reward = JsonUtils.getInt(json, "reward", 0);
                    int requiredLevel = JsonUtils.getInt(json, "level", 0);
                    String appendix = JsonUtils.getString(json, "appendix", "");
                    String name = FilenameUtils.removeExtension(root.relativize(file).toString());
                    Item drop = new ItemBase(name);
                    ModItems.register(drop);
                    register(new BlockJobResource(name + "_" + appendix, job, requiredLevel, reward, new ItemStack(drop)));
                } catch (JsonParseException e) {
                    ArthaniaMod.logger.error("Error while parsing resource for job {} : {}", job.name(), file.toString());
                    e.printStackTrace();
                } catch (IOException e) {
                    ArthaniaMod.logger.error("Error while reading contents of the resource file of {} : {})", job.name(), file.toString());
                    e.printStackTrace();
                } finally {
                    IOUtils.closeQuietly(reader);
                }
                return true;

            }, true, true);
        }

        BLOCKS.keySet().forEach(event.getRegistry()::register);
    }

    public static void register(Block block) {
        register(block, ItemBlock::new);
    }

    public static void register(Block block, Function<Block, Item> itemBlockSupplier) {
        Item item = itemBlockSupplier.apply(block);
        item.setRegistryName(block.getRegistryName());
        BLOCKS.put(block, item);
    }

    public static void registerTile(Class<? extends TileEntity> tileClass, String name) {
        GameRegistry.registerTileEntity(tileClass, ArthaniaMod.MODID + ":" + name);
    }

}
