package pvp.arthania.common.crafting;

import org.apache.commons.lang3.builder.ToStringBuilder;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class JobRecipe {

    protected ItemStack[]      ingredients;
    protected ItemStack[]      results;
    protected int              neededLevel;
    protected ResourceLocation rl;

    public JobRecipe(ResourceLocation name, ItemStack[] ingredients, ItemStack[] results, int neededLevel) {
        this.ingredients = ingredients;
        this.results = results;
        this.neededLevel = neededLevel;
        this.rl = name;
    }

    public ResourceLocation getName() {
        return this.rl;
    }

    public ItemStack[] getIngredients() {
        return ingredients;
    }

    public ItemStack[] getResults() {
        return results;
    }

    public int getNeededLevel() {
        return neededLevel;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof JobRecipe && ((JobRecipe) o).getName().equals(this.getName());
    }

    @Override
    public int hashCode() {
        return this.rl.hashCode();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
