package pvp.arthania.common.crafting;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.util.PlayerJob;

public class JobCrafting {

    public final static EnumMap<PlayerJob, Set<JobRecipe>> RECIPES = new EnumMap<>(PlayerJob.class);
    public static final Gson                              GSON    = new Gson();

    public static void loadCrafts() {
        for (PlayerJob job : PlayerJob.values()) {
            CraftingHelper.findFiles(ArthaniaMod.container, "assets/" + ArthaniaMod.MODID + "/jobs/crafting/" + job.toString().toLowerCase(), null, (root, file) -> {
                if (!"json".equals(FilenameUtils.getExtension(file.toString()))) {
                    return true;
                }

                BufferedReader reader = null;
                try {
                    reader = Files.newBufferedReader(file);
                    JsonObject json = JsonUtils.fromJson(GSON, reader, JsonObject.class);

                    JsonArray ingredients = JsonUtils.getJsonArray(json, "ingredients");
                    List<ItemStack> ingStacks = getStacks(ingredients, false);

                    JsonArray results = JsonUtils.getJsonArray(json, "results");
                    List<ItemStack> resultsStacks = getStacks(results, false);

                    int level = JsonUtils.getInt(json, "level");

                    if (!RECIPES.containsKey(job)) {
                        RECIPES.put(job, new HashSet<>());
                    }

                    String relative = root.relativize(file).toString();

                    RECIPES.get(job).add(new JobRecipe(new ResourceLocation(job.name().toLowerCase(), FilenameUtils.removeExtension(relative)), ingStacks.toArray(new ItemStack[0]),
                            resultsStacks.toArray(new ItemStack[0]), level));

                } catch (JsonParseException e) {
                    ArthaniaMod.logger.error("Error while parsing job recipe json : {}", file.toString());
                    e.printStackTrace();
                } catch (IOException e) {
                    ArthaniaMod.logger.error("Error while reading contents of the job recipe file : {})", file.toString());
                    e.printStackTrace();
                } catch (NumberFormatException e) {
                    ArthaniaMod.logger.error("Error while reading trying to read the job recipe : {}", file.toString());
                    e.printStackTrace();
                } finally {
                    IOUtils.closeQuietly(reader);
                }

                return true;
            }, true, true);
        }
    }

    private static List<ItemStack> getStacks(JsonArray array, boolean allowEmpty) {
        ArrayList<ItemStack> ingStacks = new ArrayList<>();
        for (int i = 0; i < array.size(); i++) {
            JsonElement element = array.get(i);
            if (!element.isJsonPrimitive() || !element.getAsJsonPrimitive().isString())
                throw new JsonSyntaxException("Expected stack as string");
            String[] ing = element.getAsString().split("x", 2);
            int count = Integer.parseInt(ing[0]);
            if (count < 1)
                throw new JsonSyntaxException("Expected stack count not to be less than 1, got " + count + ".");
            ResourceLocation loc = new ResourceLocation(ing[1]);
            Item item = ForgeRegistries.ITEMS.getValue(loc);
            if (item == null)
                throw new JsonSyntaxException("Couldn't find item value " + loc.toString() + ".");
            ingStacks.add(new ItemStack(item, count));
        }

        if (ingStacks.isEmpty() && !allowEmpty)
            throw new JsonParseException("Ingredients cannot be empty.");

        return ingStacks;
    }

}
