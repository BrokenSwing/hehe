package pvp.arthania.common.material;

import java.util.ArrayList;
import java.util.Collection;

import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.EntityEquipmentSlot.Type;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.common.util.EnumHelper;
import pvp.arthania.common.item.restricted.ItemRestrictedArmor;
import pvp.arthania.common.item.restricted.ItemRestrictedAxe;
import pvp.arthania.common.item.restricted.ItemRestrictedPickaxe;
import pvp.arthania.common.item.restricted.ItemRestrictedShovel;
import pvp.arthania.common.item.restricted.ItemRestrictedSword;

public class Materials {

    public static final ArmorMaterial GOBLIN_ARMOR   = createArmorMaterial("goblin", 100);
    public static final ArmorMaterial SKELETON_ARMOR = createArmorMaterial("skeleton", 100);
    public static final ArmorMaterial DRAKE_ARMOR    = createArmorMaterial("drake", 100);
    public static final ArmorMaterial ARIX_ARMOR     = createArmorMaterial("arix", 100);
    
    public static final ToolMaterial GOBLIN_TOOL = createToolMaterial("gobling", 4000, 26, 9);
    public static final ToolMaterial SKELETON_TOOL = createToolMaterial("skeleton", 400, 26, 9);
    public static final ToolMaterial DRAKE_TOOL = createToolMaterial("drake", 400, 26, 9);

    private static ArmorMaterial createArmorMaterial(String name, int durability) {
        return EnumHelper.addArmorMaterial(name, name, durability, new int[] { 4, 7, 8, 4 }, 20, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0F);
    }

    public static Collection<Item> createArmorParts(ArmorMaterial material, int requiredLevel) {
        ArrayList<Item> parts = new ArrayList<>();
        for (EntityEquipmentSlot slot : EntityEquipmentSlot.values()) {
            if (slot.getSlotType() != Type.ARMOR)
                continue;
            Item part = new ItemRestrictedArmor(material.name().toLowerCase() + "_" + slot.getName(), material, slot, requiredLevel);
            parts.add(part);
        }
        return parts;
    }
    
    private static ToolMaterial createToolMaterial(String name, int durability, float efficiency, float damages) {
        return EnumHelper.addToolMaterial(name, 3, durability, efficiency, damages, 25);
    }
    
    public static Collection<Item> createToolsSet(ToolMaterial material, int requiredLevel) {
        ArrayList<Item> tools = new ArrayList<>();
        tools.add(new ItemRestrictedAxe(material.name().toLowerCase() + "_axe", material, requiredLevel));
        tools.add(new ItemRestrictedSword(material.name().toLowerCase() + "_sword", material, requiredLevel));
        tools.add(new ItemRestrictedPickaxe(material.name().toLowerCase() + "_pickaxe", material, requiredLevel));
        tools.add(new ItemRestrictedShovel(material.name().toLowerCase() + "_shovel", material, requiredLevel));
        return tools;
    }

}
