package pvp.arthania.common.util;

public class LevelCurve {

    public static double needExperienceToLevelUp(int level) {
        return (500 * level + 130 * Math.pow(level + 1, 2.4));
    }

    public static int getJobLevelFromExperience(int experience) {
        return 1 + experience / 100;
    }

}
