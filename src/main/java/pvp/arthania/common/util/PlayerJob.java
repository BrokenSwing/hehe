package pvp.arthania.common.util;

import net.minecraft.client.resources.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public enum PlayerJob {

    ALCHEMIST, LUMBERJACK, MINER, FARMER, HUNTER, COOK, BLACKSMITH, FISHER, TAILOR;

    @SideOnly(Side.CLIENT)
    public String getTranslatedName() {
        return I18n.format("job." + name().toLowerCase() + ".name");
    }

}
