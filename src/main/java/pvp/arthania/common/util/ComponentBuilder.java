package pvp.arthania.common.util;

import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

public class ComponentBuilder {

    public static ComponentBuilder create() {
        return create(TextFormatting.WHITE);
    }

    public static ComponentBuilder create(TextFormatting textColor) {
        return create(textColor, TextFormatting.YELLOW);
    }

    public static ComponentBuilder create(TextFormatting textColor, TextFormatting dataColor) {
        return new ComponentBuilder(textColor, dataColor);
    }
    
    public static ComponentBuilder valid() {
        return create(TextFormatting.DARK_GREEN, TextFormatting.GREEN);
    }
    
    public static ComponentBuilder error() {
        return create(TextFormatting.DARK_RED, TextFormatting.RED);
    }

    private TextFormatting dataColor;

    private ITextComponent component;

    private ComponentBuilder(TextFormatting textColor, TextFormatting dataColor) {
        this.dataColor = dataColor;
        component = new TextComponentString("").setStyle(new Style().setColor(textColor));
    }

    public ComponentBuilder text(String text) {
        this.component.appendText(text);
        return this;
    }

    public ComponentBuilder translate(String translationKey, Object... args) {
        this.component.appendSibling(new TextComponentTranslation(translationKey, args));
        return this;
    }

    public ComponentBuilder data(Object o) {
        this.component.appendSibling(new TextComponentString(o.toString()).setStyle(new Style().setColor(dataColor)));
        return this;
    }
    
    public ITextComponent build() {
        return this.component;
    }

}
