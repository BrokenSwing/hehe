package pvp.arthania.common.util;

public enum PlayerClass {

    BERSERKER, TANK, ARCHER, WIZARD, SUMMONER;

}
