package pvp.arthania.common.impl;

import java.util.function.Function;

import mcjty.theoneprobe.api.IProbeHitData;
import mcjty.theoneprobe.api.IProbeInfo;
import mcjty.theoneprobe.api.IProbeInfoProvider;
import mcjty.theoneprobe.api.ITheOneProbe;
import mcjty.theoneprobe.api.ProbeMode;
import mcjty.theoneprobe.api.TextStyleClass;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.block.BlockJobResource;
import pvp.arthania.common.capability.IPlayerState;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.util.LevelCurve;

public class TOPImplementation implements IProbeInfoProvider {

    public static class Register implements Function<ITheOneProbe, Void> {

        @Override
        public Void apply(ITheOneProbe top) {
            top.registerProvider(new TOPImplementation());
            return null;
        }
        
    }

    @Override
    public String getID() {
        return ArthaniaMod.MODID + ":block";
    }

    @Override
    public void addProbeInfo(ProbeMode mode, IProbeInfo probeInfo, EntityPlayer player, World world, IBlockState blockState, IProbeHitData data) {
        IPlayerState state = player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null);
        if (blockState.getBlock() instanceof BlockJobResource) {
            BlockJobResource block = (BlockJobResource) blockState.getBlock();
            probeInfo.text((state.getJobs().contains(block.getJob()) ? TextStyleClass.OK : TextStyleClass.WARNING) + block.getJob().name());
            int jobLevel = LevelCurve.getJobLevelFromExperience(state.getExperienceOf(block.getJob()));
            probeInfo.text((jobLevel >= block.getRequiredLevel() ? TextStyleClass.OK : TextStyleClass.WARNING) + "Require level " + block.getRequiredLevel());
            probeInfo.text("Experience reward : " + block.getExperienceReward());
        }
    }

}
