package pvp.arthania.common.item;

import net.minecraft.item.Item;
import pvp.arthania.common.ArthaniaMod;

public class ItemBase extends Item {
    
    public ItemBase(String name) {
        this.setRegistryName(ArthaniaMod.MODID, name);
        this.setUnlocalizedName(name);
        this.setCreativeTab(ArthaniaMod.MOD_TAB);
    }

}
