package pvp.arthania.common.item;

import net.minecraft.item.ItemStack;
import pvp.arthania.common.item.restricted.ILevelRestrictedEquipment;

public class ItemTest extends ItemBase implements ILevelRestrictedEquipment {

    public ItemTest(String name) {
        super(name);
    }

    @Override
    public int getRequiredLevel(ItemStack stack) {
        return 10;
    }

}
