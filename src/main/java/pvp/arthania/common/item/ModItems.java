package pvp.arthania.common.item;

import java.util.ArrayList;
import java.util.Collection;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.block.ModBlocks;
import pvp.arthania.common.material.Materials;

@EventBusSubscriber(modid = ArthaniaMod.MODID)
public class ModItems {

    @ObjectHolder(ArthaniaMod.MODID + ":admin_wand")
    public static final Item ADMIN_WAND = Items.AIR;

    public static final ArrayList<Item> ITEMS = new ArrayList<>();

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        register(new ItemBase("admin_wand"));
        register(new ItemTest("test"));

        // Armors
        register(Materials.createArmorParts(Materials.GOBLIN_ARMOR, 1));
        register(Materials.createArmorParts(Materials.SKELETON_ARMOR, 10));
        register(Materials.createArmorParts(Materials.DRAKE_ARMOR, 10));
        register(Materials.createArmorParts(Materials.ARIX_ARMOR, 10));
        
        // Tools
        register(Materials.createToolsSet(Materials.GOBLIN_TOOL, 1));
        register(Materials.createToolsSet(Materials.SKELETON_TOOL, 10));
        register(Materials.createToolsSet(Materials.DRAKE_TOOL, 10));
        
        ITEMS.addAll(ModBlocks.BLOCKS.values());
        ITEMS.forEach(event.getRegistry()::register);
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event) {
        ITEMS.forEach(item -> {
            ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory"));
        });
    }

    public static void register(Item item) {
        ITEMS.add(item);
    }
    
    public static void register(Collection<Item> items) {
        ITEMS.addAll(items);
    }

}
