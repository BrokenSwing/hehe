package pvp.arthania.common.item.restricted;

import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.capability.PlayerStateProvider;

public class ItemRestrictedShovel extends ItemSpade implements ILevelRestrictedEquipment {

    protected int requiredLevel;

    public ItemRestrictedShovel(String name, ToolMaterial material, int requiredLevel) {
        super(material);
        this.setRegistryName(ArthaniaMod.MODID, name);
        this.setUnlocalizedName(name);
        this.setCreativeTab(ArthaniaMod.MOD_TAB);
        this.requiredLevel = requiredLevel;
    }

    @Override
    public int getRequiredLevel(ItemStack stack) {
        return this.requiredLevel;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        TextFormatting color;
        if (Minecraft.getMinecraft().player != null)
            color = Minecraft.getMinecraft().player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null).getLevel() >= this.getRequiredLevel(stack) ? TextFormatting.GREEN : TextFormatting.RED;
        else
            color = TextFormatting.WHITE;
        tooltip.add(color + I18n.format("equipment.required_level", this.getRequiredLevel(stack)));
    }

}
