package pvp.arthania.common.item.restricted;

import net.minecraft.item.ItemStack;

public interface ILevelRestrictedEquipment {

    public int getRequiredLevel(ItemStack stack);

}
