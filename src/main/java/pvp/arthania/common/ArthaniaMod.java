package pvp.arthania.common;

import org.apache.logging.log4j.Logger;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import pvp.arthania.common.proxy.CommonProxy;

@Mod(modid = ArthaniaMod.MODID, version = ArthaniaMod.VERSION, name = ArthaniaMod.NAME)
public class ArthaniaMod {

    public static final String MODID   = "arthania";
    public static final String NAME    = "Arthania mod";
    public static final String VERSION = "2.0.0";
    
    public static final CreativeTabs MOD_TAB = new CreativeTabs(NAME) {
        
        @Override
        public ItemStack getTabIconItem() {
            return new ItemStack(Item.getItemFromBlock(Blocks.BEDROCK));
        }
    };
    
    @Instance(MODID)
    public static Object instance = null;
    
    public static Logger logger = null;
    public static ModContainer container;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        container = Loader.instance().activeModContainer();
        CommonProxy.instance.preInit(event);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
        CommonProxy.instance.init(event);
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        CommonProxy.instance.postInit(event);
    }
    
    @EventHandler
    public void serverStarting(FMLServerStartingEvent event) {
        CommonProxy.instance.serverStarting(event);
    }
    
    @EventHandler
    public void serverStarted(FMLServerStartedEvent event) {
        CommonProxy.instance.serverStarted(event);
    }

}
