package pvp.arthania.common.capability;

import java.util.EnumSet;

import pvp.arthania.common.util.PlayerClass;
import pvp.arthania.common.util.PlayerFaction;
import pvp.arthania.common.util.PlayerJob;

public interface IPlayerState {

    // DIRTY MARKING //

    /**
     * Fires a sync with the client side (called each time a value is modified)
     */
    public void markDirty();

    /**
     * Used to prevent server to send capabilities updates when reading them from
     * NBT.
     * 
     * @param initializing
     *            Is the capability being loaded from the NBT
     */
    public void setInitializing(boolean initializing);

    // PLAYER CLASS //

    public PlayerClass getPlayerClass();

    public void setPlayerClass(PlayerClass type);

    // LEVEL //

    public int getLevel();

    default public void addLevel(int level) {
        this.setLevel(this.getLevel() + level);
    }

    public void setLevel(int level);

    // XP //

    public long getXp();

    default public void addXp(long xp) {
        this.setXp(this.getXp() + xp);
    }

    public void setXp(long xp);

    // MONEY //

    public int getMoney();

    public void setMoney(int amount);

    default public void consumeMoney(int amount) {
        this.addMoney(-amount);
    }

    default public void addMoney(int amount) {
        this.setMoney(this.getMoney() + amount);
    }

    // HEALTH //

    public int getHealth();

    public void setHeath(int life);

    default public void addHealth(int amount) {
        this.setHeath(this.getHealth() + amount);
    }

    default public void consumeHealth(int amount) {
        this.addHealth(-amount);
    }

    // DONGEON //

    public String getBossDungeonKill();

    public void setBossDungeonKill(String bossKilled);

    public void resetBossDungeonKill(String newBossList);

    // STRENGTH //

    public int getStrength();

    public void setStrength(int strength);

    default public void addStrength(int strength) {
        this.setStrength(this.getStrength() + strength);
    }

    default public void consumeStrength(int strength) {
        this.addStrength(-strength);
    }

    // RESISTANCE //

    public int getResistance();

    public void setResistance(int resistance);

    default public void addResistance(int resistance) {
        this.setResistance(this.getResistance() + resistance);
    }

    default public void consumeResistance(int resistance) {
        this.addResistance(-resistance);
    }

    // SPEED //

    public int getSpeed();

    public void setSpeed(int speed);

    default public void addSpeed(int speed) {
        this.setSpeed(this.getSpeed() + speed);
    }

    default public void consumeSpeed(int speed) {
        this.addSpeed(-speed);
    }

    // BASE //

    public int getBase();

    public void setBase(int base);

    default public void addBase(int base) {
        this.setBase(this.getBase() + base);
    }

    default public void consumeBase(int base) {
        this.addBase(-base);
    }

    // BOUTIQUE POINTS //

    public void setBoutiquePoints(int pointBoutique);

    public int getBoutiquePoints();

    default public void addPointBoutique(int pointBoutique) {
        this.setBoutiquePoints(this.getBoutiquePoints() + pointBoutique);
    }

    default public void consumePointBoutique(int pointBoutique) {
        this.addPointBoutique(-pointBoutique);
    }

    // JOBS //

    public EnumSet<PlayerJob> getJobs();

    /**
     * Adds a job to the player.
     * 
     * @param job
     *            The job to add
     * @return true if the player didn't have the job, else false
     */
    public boolean addJob(PlayerJob job);

    /**
     * Removes a job to the player.
     * 
     * @param job
     *            The job to remove
     * @return true if the player had the job, else false
     */
    public boolean removeJob(PlayerJob job);

    public void setExperienceOf(PlayerJob job, int experience);

    public int getExperienceOf(PlayerJob job);

    default public void addExperienceTo(PlayerJob job, int experience) {
        this.setExperienceOf(job, this.getExperienceOf(job) + experience);
    }

    // FACTION //

    public void setPlayerFaction(PlayerFaction faction);

    public PlayerFaction getPlayerFaction();
}
