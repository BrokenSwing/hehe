package pvp.arthania.common.capability;

import java.util.EnumMap;
import java.util.EnumSet;

import pvp.arthania.common.util.PlayerClass;
import pvp.arthania.common.util.PlayerFaction;
import pvp.arthania.common.util.PlayerJob;

public class DefaultPlayerState implements IPlayerState {

    public DefaultPlayerState() {
        this.level = 1;
        this.xp = 0L;
        this.base = 0;
        this.boutiquePoints = 0;
        this.health = 20;
        this.strength = 1;
        this.money = 0;
        this.resistance = 0;
        this.speed = 1;
        this.dungeonKill = "";
        this.playerClass = null;
        this.faction = null;
        this.initializingCapa = false;
    }

    // PLAYER CLASS //

    protected PlayerClass playerClass;

    @Override
    public PlayerClass getPlayerClass() {
        return this.playerClass;
    }

    @Override
    public void setPlayerClass(PlayerClass type) {
        this.playerClass = type;
        this.markDirty();
    }

    // LEVEL //

    protected int level;

    @Override
    public int getLevel() {
        return this.level;
    }

    @Override
    public void setLevel(int level) {
        this.level = level;
        this.markDirty();
    }

    // XP //

    protected long xp;

    @Override
    public long getXp() {
        return this.xp;
    }

    @Override
    public void setXp(long xp) {
        this.xp = xp;
        this.markDirty();
    }

    // MONEY //

    protected int money;

    @Override
    public int getMoney() {
        return this.money;
    }

    @Override
    public void setMoney(int amount) {
        this.money = amount;
        this.markDirty();
    }

    // HEALTH //

    protected int health;

    @Override
    public int getHealth() {
        return this.health;
    }

    @Override
    public void setHeath(int life) {
        this.health = life;
        this.markDirty();
    }

    // DUNGEON //

    protected String dungeonKill;

    @Override
    public String getBossDungeonKill() {
        return this.dungeonKill;
    }

    @Override
    public void setBossDungeonKill(String bossKilled) {
        this.dungeonKill = bossKilled;
        this.markDirty();
    }

    @Override
    public void resetBossDungeonKill(String newBossList) {
        this.dungeonKill = newBossList;
        this.markDirty();
    }

    // STRENGTH //

    protected int strength;

    @Override
    public int getStrength() {
        return this.strength;
    }

    @Override
    public void setStrength(int strength) {
        this.strength = strength;
        this.markDirty();
    }

    // RESISTANCE //

    protected int resistance;

    @Override
    public int getResistance() {
        return this.resistance;
    }

    @Override
    public void setResistance(int resistance) {
        this.resistance = resistance;
        this.markDirty();
    }

    // SPEED //

    protected int speed;

    @Override
    public int getSpeed() {
        return this.speed;
    }

    @Override
    public void setSpeed(int speed) {
        this.speed = speed;
        this.markDirty();
    }

    // BASE //

    protected int base;

    @Override
    public int getBase() {
        return this.base;
    }

    @Override
    public void setBase(int base) {
        this.base = base;
        this.markDirty();
    }

    // BOUTIQUE POINTS //

    protected int boutiquePoints;

    @Override
    public void setBoutiquePoints(int pointBoutique) {
        this.boutiquePoints = pointBoutique;
        this.markDirty();
    }

    @Override
    public int getBoutiquePoints() {
        return this.boutiquePoints;
    }

    // JOBS //

    protected EnumSet<PlayerJob> jobs = EnumSet.noneOf(PlayerJob.class);

    @Override
    public EnumSet<PlayerJob> getJobs() {
        return this.jobs;
    }

    @Override
    public boolean addJob(PlayerJob job) {
        if (this.jobs.add(job)) {
            this.markDirty();
            return true;
        }
        return false;
    }

    @Override
    public boolean removeJob(PlayerJob job) {
        if (this.jobs.remove(job)) {
            this.markDirty();
            return true;
        }
        return false;
    }

    // JOBS EXPERIENCE //

    protected EnumMap<PlayerJob, Integer> jobsExperience = new EnumMap<>(PlayerJob.class);
    {
        for (PlayerJob j : PlayerJob.values())
            jobsExperience.put(j, new Integer(0));
    }

    @Override
    public void setExperienceOf(PlayerJob job, int experience) {
        this.jobsExperience.put(job, experience);
        this.markDirty();
    }

    @Override
    public int getExperienceOf(PlayerJob job) {
        return jobsExperience.get(job);
    }

    // FACTION //

    protected PlayerFaction faction;

    @Override
    public void setPlayerFaction(PlayerFaction faction) {
        this.faction = faction;
        this.markDirty();
    }

    @Override
    public PlayerFaction getPlayerFaction() {
        return this.faction;
    }

    // DIRTY MARKING //

    protected boolean initializingCapa;

    @Override
    public void setInitializing(boolean initializing) {
        this.initializingCapa = initializing;
    }

    @Override
    public void markDirty() {}

}
