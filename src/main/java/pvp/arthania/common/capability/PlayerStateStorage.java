package pvp.arthania.common.capability;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.util.Constants.NBT;
import pvp.arthania.common.util.PlayerClass;
import pvp.arthania.common.util.PlayerFaction;
import pvp.arthania.common.util.PlayerJob;

public class PlayerStateStorage implements IStorage<IPlayerState> {

    @Override
    public NBTBase writeNBT(Capability<IPlayerState> capability, IPlayerState instance, EnumFacing side) {
        NBTTagCompound nbt = new NBTTagCompound();
        if (instance.getPlayerClass() != null) {
            nbt.setInteger("player_class", instance.getPlayerClass().ordinal());
        }
        if (instance.getPlayerFaction() != null) {
            nbt.setInteger("player_faction", instance.getPlayerFaction().ordinal());
        }
        nbt.setInteger("level", instance.getLevel());
        nbt.setLong("experience", instance.getXp());
        nbt.setInteger("money", instance.getMoney());
        nbt.setInteger("health", instance.getHealth());
        nbt.setString("dungeon", instance.getBossDungeonKill());
        nbt.setInteger("strength", instance.getStrength());
        nbt.setInteger("resistance", instance.getResistance());
        nbt.setInteger("speed", instance.getSpeed());
        nbt.setInteger("base", instance.getBase());
        nbt.setInteger("boutique_points", instance.getBoutiquePoints());

        NBTTagList jobsList = new NBTTagList();
        instance.getJobs().forEach(j -> jobsList.appendTag(new NBTTagInt(j.ordinal())));
        nbt.setTag("jobs", jobsList);
        
        NBTTagList expList = new NBTTagList();
        for(PlayerJob job : PlayerJob.values()) {
            expList.appendTag(new NBTTagInt(instance.getExperienceOf(job)));
        }
        nbt.setTag("jobs_experience", expList);
        
        return nbt;
    }

    @Override
    public void readNBT(Capability<IPlayerState> capability, IPlayerState state, EnumFacing side, NBTBase base) {
        if (!(base instanceof NBTTagCompound))
            return;
        NBTTagCompound nbt = (NBTTagCompound) base;

        state.setInitializing(true);

        if (nbt.hasKey("player_class", NBT.TAG_INT))
            state.setPlayerClass(PlayerClass.values()[nbt.getInteger("player_class")]);
        else
            state.setPlayerClass(null);

        if (nbt.hasKey("player_faction", NBT.TAG_INT))
            state.setPlayerFaction(PlayerFaction.values()[nbt.getInteger("player_faction")]);
        else
            state.setPlayerFaction(null);

        state.setLevel(nbt.getInteger("level"));
        state.setXp(nbt.getLong("experience"));
        state.setMoney(nbt.getInteger("money"));
        state.setHeath(nbt.getInteger("health"));
        state.setBossDungeonKill(nbt.getString("dungeon"));
        state.setStrength(nbt.getInteger("strength"));
        state.setResistance(nbt.getInteger("resistance"));
        state.setSpeed(nbt.getInteger("speed"));
        state.setBase(nbt.getInteger("base"));
        state.setBoutiquePoints(nbt.getInteger("boutique_points"));

        NBTTagList jobs = nbt.getTagList("jobs", NBT.TAG_INT);
        for(int i = 0; i < jobs.tagCount(); i++) {
            state.addJob(PlayerJob.values()[jobs.getIntAt(i)]);
        }

        NBTTagList exp = nbt.getTagList("jobs_experience", NBT.TAG_INT);
        for (int i = 0; i < exp.tagCount(); i++) {
            state.setExperienceOf(PlayerJob.values()[i], exp.getIntAt(i));
        }

        state.setInitializing(false);
    }

}
