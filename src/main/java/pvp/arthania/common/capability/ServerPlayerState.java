package pvp.arthania.common.capability;

import net.minecraft.entity.player.EntityPlayerMP;
import pvp.arthania.common.network.ModNetwork;
import pvp.arthania.common.network.packets.SPacketUpdateCapability;

public class ServerPlayerState extends DefaultPlayerState {

    protected final EntityPlayerMP player;

    public ServerPlayerState(EntityPlayerMP player) {
        this.player = player;
    }

    @Override
    public void markDirty() {
        if (!this.initializingCapa) {
            ModNetwork.NETWORK.sendTo(new SPacketUpdateCapability(this), player);
        }
    }

}
