package pvp.arthania.common.capability;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import pvp.arthania.common.ArthaniaMod;

@EventBusSubscriber(modid = ArthaniaMod.MODID)
public class PlayerStateProvider implements ICapabilitySerializable<NBTBase> {

    @CapabilityInject(IPlayerState.class)
    public static final Capability<IPlayerState> PLAYER_STATE_CAPA = null;

    private final IPlayerState playerState;

    public PlayerStateProvider(EntityPlayer player, Side side) {
        this.playerState = side.isClient() ? new DefaultPlayerState() : new ServerPlayerState((EntityPlayerMP) player);
    }

    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
        return capability == PLAYER_STATE_CAPA;
    }

    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
        return this.hasCapability(capability, facing) ? PLAYER_STATE_CAPA.cast(this.playerState) : null;
    }

    @Override
    public NBTBase serializeNBT() {
        return PLAYER_STATE_CAPA.writeNBT(this.playerState, null);
    }

    @Override
    public void deserializeNBT(NBTBase nbt) {
        PLAYER_STATE_CAPA.readNBT(this.playerState, null, nbt);
    }

    public static final ResourceLocation CAP_LOC = new ResourceLocation(ArthaniaMod.MODID, "player_state");

    @SubscribeEvent
    public static void attachCapability(AttachCapabilitiesEvent<Entity> event) {
        if (event.getObject() instanceof EntityPlayer) {
            event.addCapability(CAP_LOC, new PlayerStateProvider((EntityPlayer) event.getObject(), event.getObject().world.isRemote ? Side.CLIENT : Side.SERVER));
        }
    }

    @SubscribeEvent
    public static void keepCapability(PlayerEvent.Clone event) {
        if (event.isWasDeath()) {
            NBTBase nbt = PLAYER_STATE_CAPA.writeNBT(event.getOriginal().getCapability(PLAYER_STATE_CAPA, null), null);
            PLAYER_STATE_CAPA.readNBT(event.getEntityPlayer().getCapability(PLAYER_STATE_CAPA, null), null, nbt);
        }
    }

    @SubscribeEvent
    public static void joinWorld(EntityJoinWorldEvent event) {
        if (event.getEntity() instanceof EntityPlayer) {
            ((EntityPlayer) event.getEntity()).getCapability(PLAYER_STATE_CAPA, null).markDirty();
        }
    }

}
