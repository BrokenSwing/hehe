package pvp.arthania.common.proxy;

import java.io.File;

import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.capability.DefaultPlayerState;
import pvp.arthania.common.capability.IPlayerState;
import pvp.arthania.common.capability.PlayerStateStorage;
import pvp.arthania.common.config.CommonConfig;
import pvp.arthania.common.crafting.JobCrafting;
import pvp.arthania.common.network.ModNetwork;

public abstract class CommonProxy {

    @SidedProxy(modId = ArthaniaMod.MODID, clientSide = "pvp.arthania.client.proxy.ClientProxy", serverSide = "pvp.arthania.server.proxy.ServerProxy")
    public static CommonProxy instance = null;

    protected CommonConfig config;

    public void preInit(FMLPreInitializationEvent event) {
        this.config = createConfig(event.getSuggestedConfigurationFile(), "1.0.0");
        NetworkRegistry.INSTANCE.registerGuiHandler(ArthaniaMod.instance, new ModNetwork());
        this.registerCapabilities();
        FMLInterModComms.sendFunctionMessage("theoneprobe", "getTheOneProbe", "pvp.arthania.common.impl.TOPImplementation$Register");
    }

    public void init(FMLInitializationEvent event) {
        ModNetwork.registerPackets();
    }

    public void postInit(FMLPostInitializationEvent event) {
        JobCrafting.loadCrafts();
    }

    public void serverStarting(FMLServerStartingEvent event) {

    }

    public void serverStarted(FMLServerStartedEvent event) {

    }

    public CommonConfig getConfig() {
        return this.config;
    }

    protected abstract CommonConfig createConfig(File configFile, String version);

    public void registerCapabilities() {
        CapabilityManager.INSTANCE.register(IPlayerState.class, new PlayerStateStorage(), DefaultPlayerState::new);
    }

}
