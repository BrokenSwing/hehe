package pvp.arthania.common.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.math.BlockPos;

public class EmptyContainer extends Container {
    
    private final BlockPos pos;
    
    public EmptyContainer(BlockPos pos) {
        this.pos = pos;
    }

    @Override
    public boolean canInteractWith(EntityPlayer player) {
        return player.getPosition().distanceSq(this.pos) < 25d;
    }

}
