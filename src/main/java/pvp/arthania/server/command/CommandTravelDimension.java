package pvp.arthania.server.command;

import java.util.List;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.util.ComponentBuilder;
import scala.actors.threadpool.Arrays;

public class CommandTravelDimension extends CommandBase {

    public CommandTravelDimension(String name, DefaultPermissionLevel permLevel, String commandDescription) {
        super(name, permLevel, commandDescription);
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return String.format("/%s <dimension>", this.getName());
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0)
            throw new WrongUsageException(this.getUsage(sender));
        EntityPlayerMP player = getCommandSenderAsPlayer(sender);
        int dim = parseInt(args[0]);
        if (player.world.provider.getDimension() == dim) {
            player.sendMessage(ComponentBuilder.error().text("You're already in the dimension ").data(dim).text(".").build());
        } else if (!DimensionManager.isDimensionRegistered(dim)) {
            player.sendMessage(ComponentBuilder.error().text("Dimension with id ").data(dim).text(" does not exist.").build());
        } else {
            player.sendMessage(ComponentBuilder.valid().text("You'll be teleported in the dimension ").data(dim).text(".").build());
            server.getPlayerList().changePlayerDimension(player, dim);
            player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null).markDirty();
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos targetPos) {
        if (args.length == 1) {
            return getListOfStringsMatchingLastWord(args, Arrays.asList(DimensionManager.getStaticDimensionIDs()));
        }
        return null;
    }

}
