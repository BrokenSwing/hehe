package pvp.arthania.server.command;

import java.util.EnumSet;
import java.util.List;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import pvp.arthania.common.capability.IPlayerState;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.util.ComponentBuilder;
import pvp.arthania.common.util.PlayerJob;

public class CommandJob extends CommandBase {

    public CommandJob(String name, DefaultPermissionLevel permLevel, String commandDescription) {
        super(name, permLevel, commandDescription);
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return String.format("/%s <player> <add | remove | list> <job>", this.getName());
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length < 2)
            throw new WrongUsageException(this.getUsage(sender));

        EntityPlayerMP player = getPlayer(server, sender, args[0]);
        IPlayerState state = player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null);

        if (args[1].equals("list")) {
            if (state.getJobs().isEmpty()) {
                sender.sendMessage(ComponentBuilder.valid().text("The player ").data(player.getName()).text(" has no jobs.").build());
            } else {
                String jobs = Joiner.on(", ").join(state.getJobs());
                sender.sendMessage(ComponentBuilder.valid().data(player.getName()).text("' jobs : ").data(jobs).text(".").build());
            }
        } else if (args.length < 3) {
            throw new WrongUsageException(this.getUsage(sender));
        } else {
            PlayerJob job = getEnumValueFromName(PlayerJob.class, args[2]);
            if (job == null)
                throw new WrongUsageException("%s is not a valid job.", args[2]);
            if (args[1].equals("add")) {
                if (state.addJob(job)) {
                    sender.sendMessage(ComponentBuilder.valid().text("The player ").data(player.getName()).text(" now has the job ").data(job).text(".").build());
                } else {
                    sender.sendMessage(ComponentBuilder.error().text("The player").data(player.getName()).text(" already has the job").data(job).build());
                }
            } else if (args[1].equals("remove")) {
                if (state.removeJob(job)) {
                    sender.sendMessage(ComponentBuilder.valid().text("The player ").data(player.getName()).text(" doesn't have the job ").data(job).text(" anymore.").build());
                } else {
                    sender.sendMessage(ComponentBuilder.error().text("The player ").data(player.getName()).text(" doesn't have the job ").data(job).text(".").build());
                }
            } else {
                throw new WrongUsageException(this.getUsage(sender));
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos targetPos) {
        if (args.length == 1) {
            return CommandBase.getListOfStringsMatchingLastWord(args, server.getPlayerList().getOnlinePlayerNames());
        }
        if (args.length == 2) {
            return CommandBase.getListOfStringsMatchingLastWord(args, new String[] { "add", "remove", "list" });
        }
        if (args.length == 3 && (args[1].equals("add") || args[1].equals("remove"))) {
            return CommandBase.getListOfStringsMatchingLastWord(args, EnumSet.allOf(PlayerJob.class));
        }
        return Lists.newArrayList();
    }

}
