package pvp.arthania.server.command;

import java.util.EnumSet;
import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import pvp.arthania.common.capability.IPlayerState;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.util.ComponentBuilder;
import pvp.arthania.common.util.PlayerClass;

public class CommandPlayerClass extends CommandBase {

    public CommandPlayerClass(String name, DefaultPermissionLevel permLevel, String commandDescription) {
        super(name, permLevel, commandDescription);
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return String.format("/%s <player> <reset | set>", this.getName());
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length < 2)
            throw new WrongUsageException(this.getUsage(sender));
        EntityPlayerMP player = CommandBase.getPlayer(server, sender, args[0]);
        IPlayerState state = player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null);
        if ("reset".equals(args[1])) {
            if (state.getPlayerClass() == null) {
                sender.sendMessage(ComponentBuilder.error().text("Can't reset ").data(player.getName()).text("'s class because he has no class.").build());
            } else {
                state.setPlayerClass(null);
                sender.sendMessage(ComponentBuilder.valid().data(player.getName()).text(" no longer has class.").build());
            }
        } else if ("set".equals(args[1])) {
            if (args.length < 3)
                throw new WrongUsageException("/%s set <player class>", getName());
            PlayerClass toSet = getEnumValueFromName(PlayerClass.class, args[2]);
            if (toSet == null)
                throw new WrongUsageException("%s is not a valid class.", args[2]);
            if (state.getPlayerClass() == toSet) {
                sender.sendMessage(ComponentBuilder.error().data(player.getName()).text("' class is already ").data(toSet).text(".").build());
            } else {
                state.setPlayerClass(toSet);
                sender.sendMessage(ComponentBuilder.valid().data(player.getName()).text("' class is now ").data(toSet).text(".").build());
            }
        } else {
            throw new WrongUsageException(this.getUsage(sender));
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos targetPos) {
        if (args.length == 1) {
            return CommandBase.getListOfStringsMatchingLastWord(args, server.getPlayerList().getOnlinePlayerNames());
        }
        if (args.length == 2) {
            return CommandBase.getListOfStringsMatchingLastWord(args, new String[] { "set", "reset" });
        }
        if (args.length == 3 && (args[1].equals("set"))) {
            return CommandBase.getListOfStringsMatchingLastWord(args, EnumSet.allOf(PlayerClass.class));
        }
        return Lists.newArrayList();
    }

}
