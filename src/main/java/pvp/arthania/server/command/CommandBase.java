package pvp.arthania.server.command;

import java.util.List;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import net.minecraftforge.server.permission.PermissionAPI;
import pvp.arthania.common.ArthaniaMod;

public abstract class CommandBase extends net.minecraft.command.CommandBase {

    private final String   name;
    protected final String PERM_NODE;

    public CommandBase(String name, DefaultPermissionLevel permLevel, String commandDescription) {
        this.name = name;
        this.PERM_NODE = ArthaniaMod.MODID + ".command." + name;
        PermissionAPI.registerNode(PERM_NODE, permLevel, commandDescription);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 0;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return sender instanceof EntityPlayer ? PermissionAPI.hasPermission((EntityPlayer) sender, PERM_NODE) : true;
    }

    @Override
    public abstract List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos targetPos);

    public static <A extends Enum<A>> A getEnumValueFromName(Class<A> enumClass, String name) {
        for (A a : enumClass.getEnumConstants()) {
            if (a.name().equals(name)) {
                return a;
            }
        }
        return null;
    }

}
