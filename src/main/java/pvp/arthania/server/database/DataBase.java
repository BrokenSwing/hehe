package pvp.arthania.server.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import net.minecraftforge.fml.server.FMLServerHandler;
import pvp.arthania.common.ArthaniaMod;

public class DataBase {

    private static volatile DataBase instance = null;

    public static DataBase instance() {
        if (instance == null) {
            synchronized (DataBase.class) {
                if (instance == null) {
                    instance = new DataBase();
                }
            }
        }
        return instance;
    }
    
    private String url;
    private String username;
    private String password;
    
    private Connection connection;
    
    public void setIdentifiants(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.connection = null;
    }
    
    protected void connect() {
        if(connection == null) {
            try {
                connection = DriverManager.getConnection(this.url, this.username, this.password);
            } catch (SQLException e) {
                ArthaniaMod.logger.error("Unable to connect to the database. Url : {}, username : {}, password : {}", this.url, this.username, this.password);
                e.printStackTrace();
                FMLServerHandler.instance().getServer().stopServer();
            }
        }
    }
    
    public Connection getConnection() {
        connect();
        return this.connection;
    }

}
