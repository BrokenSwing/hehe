package pvp.arthania.server.config;

import java.io.File;

import pvp.arthania.common.config.CommonConfig;
import pvp.arthania.server.database.DataBase;

public class ServerConfig extends CommonConfig {

    public ServerConfig(File file, String version) {
        super(file, version);
    }

    @Override
    public void load() {
        super.load();
        
        String catagory = "database";
        cfg.setCategoryComment(catagory, "This category indicates how to connect to the database.");
        String url = cfg.getString("url", catagory, "", "URL access to the database.");
        String username = cfg.getString("username", catagory, "", "Username to use to connect to the database.");
        String password = cfg.getString("password", catagory, "", "Password of the specified username to use to connect to the database.");
        DataBase.instance().setIdentifiants(url, username, password);
    }

}
