package pvp.arthania.server.handler;

import java.util.IdentityHashMap;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.event.entity.living.LivingEquipmentChangeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ServerTickEvent;
import net.minecraftforge.fml.relauncher.Side;
import pvp.arthania.common.ArthaniaMod;
import pvp.arthania.common.capability.PlayerStateProvider;
import pvp.arthania.common.item.restricted.ILevelRestrictedEquipment;
import pvp.arthania.common.network.packets.SPacketToastGui;

@EventBusSubscriber(modid = ArthaniaMod.MODID, value = Side.SERVER)
public class EquipmentHandler {

    private static final IdentityHashMap<EntityPlayer, EntityEquipmentSlot> TO_REMOVE = new IdentityHashMap<>();

    @SubscribeEvent
    public static void changeEquipment(LivingEquipmentChangeEvent event) {
        if (event.getEntity() instanceof EntityPlayerMP && event.getSlot().getSlotType() == EntityEquipmentSlot.Type.ARMOR) {
            EntityPlayerMP player = (EntityPlayerMP) event.getEntity();

            if (event.getTo().getItem() instanceof ILevelRestrictedEquipment) {
                ILevelRestrictedEquipment equipment = (ILevelRestrictedEquipment) event.getTo().getItem();
                int requiredLevel = equipment.getRequiredLevel(event.getTo());
                if (player.getCapability(PlayerStateProvider.PLAYER_STATE_CAPA, null).getLevel() < requiredLevel) {
                    new SPacketToastGui(
                            new TextComponentTranslation("equipment.too_low_level"),
                            new TextComponentTranslation("equipment.required_level", requiredLevel))
                                    .sendToPlayer(player);
                    TO_REMOVE.put(player, event.getSlot());
                }
            }
        }
    }

    @SubscribeEvent
    public static void serverTick(ServerTickEvent event) {
        TO_REMOVE.forEach((player, slot) -> {
            ItemStack stack = player.inventory.armorInventory.get(slot.getIndex());
            player.inventory.armorInventory.set(slot.getIndex(), ItemStack.EMPTY);
            if (!player.capabilities.isCreativeMode && !player.inventory.addItemStackToInventory(stack)) {
                BlockPos p = player.getPosition();
                EntityItem drop = new EntityItem(player.getEntityWorld(), p.getX(), p.getY(), p.getZ(), stack);
                drop.setPickupDelay(10);
                drop.setOwner(player.getName());
                player.getEntityWorld().spawnEntity(drop);
            }
        });
        TO_REMOVE.clear();
    }

}
