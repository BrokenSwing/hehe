package pvp.arthania.server.proxy;

import java.io.File;

import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.server.FMLServerHandler;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import pvp.arthania.common.config.CommonConfig;
import pvp.arthania.common.proxy.CommonProxy;
import pvp.arthania.server.command.CommandJob;
import pvp.arthania.server.command.CommandPlayerClass;
import pvp.arthania.server.command.CommandPlayerFaction;
import pvp.arthania.server.command.CommandTravelDimension;
import pvp.arthania.server.config.ServerConfig;

public class ServerProxy extends CommonProxy {

    @Override
    protected CommonConfig createConfig(File configFile, String version) {
        return new ServerConfig(configFile, version);
    }

    @Override
    public void serverStarting(FMLServerStartingEvent event) {
        super.serverStarting(event);
        event.registerServerCommand(new CommandJob("job", DefaultPermissionLevel.OP, "Allows to modify jobs of a specified player."));
        event.registerServerCommand(new CommandPlayerClass("playerclass", DefaultPermissionLevel.OP, "Allows to modify the class of a specified player."));
        event.registerServerCommand(new CommandPlayerFaction("playerfaction", DefaultPermissionLevel.OP, "Allows to modifu the faction of a specified player."));
        event.registerServerCommand(new CommandTravelDimension("travel", DefaultPermissionLevel.OP, "Allows to travel between dimensions."));
    }

    @Override
    public void serverStarted(FMLServerStartedEvent event) {
        super.serverStarted(event);
        setupWorldGamerules(FMLServerHandler.instance().getServer().getWorld(0));
    }

    protected void setupWorldGamerules(World world) {
        GameRules gamerules = world.getGameRules();
        gamerules.setOrCreateGameRule("keepInventory", "true");
        gamerules.setOrCreateGameRule("doFireTick", "false");
        gamerules.setOrCreateGameRule("mobGriefing", "false");
        gamerules.setOrCreateGameRule("showDeathMessages", "false");
    }

}
